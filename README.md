# goCobra

GO-приложение, использующее CLI-технологию Cobra. Работает по типу телефонной (адресной) книги.
Номер телефона является уникальным полем - добавить две записи с одинаковыми номерами невозможно.

### Использование
Все команды используются в формате go run main.go {command} [arg1, arg2, ..., argN]
Применять команды необходимо, находясь в каталоге проекта.

### Формат команд
#### addContact
addContact номерТелефона имя город улица дом [квартира]<br>
пример: go run main.go addContact +380994445666 Taras Dnipro Titova 26a 45
#### findAll
findAll<br>
пример: go run main.go findAll
#### findByNumber
findByNumber номерТелефона<br>
пример: go run main.go findByNumber +380994445666
#### editContact
editContact номерТелефона имя город улица дом [квартира]<br>
пример: go run main.go editContact +380994445666 Taras Kyiv Shevchenka 15
#### deleteContact
deleteContact номерТелефона<br>
пример: go run main.go deleteContact +380994445666

#### Полезное
В папке проекта есть файл TESTphoneNumbers.txt с несколькими уже готовыми записями.
Для более быстрого тестирования функций можно переименовать файл в phoneNumbers.txt и использовать его
(т.е. не нужно будет сначала добавлять номера, чтобы потом редактировать или удалять). 